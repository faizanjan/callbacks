const fs = require('fs');

function createRandJsonDir(dirPath, fileCount, cb, overwrite=false){
    
    if(!fs.existsSync(dirPath) || (overwrite)){
        fs.mkdir(dirPath,()=>{
            console.log(`Writing ${fileCount} files in ${dirPath}`)
            for(let i=1; i<=fileCount; i++){
                fs.writeFile(`${dirPath}/randFile${i}.JSON`, `${i}` ,()=>{
                    // console.log(`randFile${i}.JSON created`)
                })
            }
            cb(dirPath); // callback called after the directory with random json files is created
        });
        return dirPath;
    }
    else{
        throw new Error(`Directory ${dirPath} already exists. Please provide a new path or specify if you want to overwrite.`);
    }
}

function deleteFilesInDir(dirPath){
    fs.readdir(dirPath, (error, files)=>{
        if(error) throw new Error (error);
        else {
            console.log(`Deleting ${files.length} files in ${dirPath}`)
            files.forEach(file=>{
                fs.unlink(`${dirPath}/${file}`, (error)=>{
                    if(error) throw new Error(error);
                    // console.log(`${file} deleted`);
                })
            })
        }
    })
}

module.exports = {createRandJsonDir, deleteFilesInDir};