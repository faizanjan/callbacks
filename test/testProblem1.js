const {createRandJsonDir, deleteFilesInDir} = require('../problem1');

const newDirPath = "./newDir/";
const fileCount = 3;

createRandJsonDir(newDirPath, fileCount, deleteFilesInDir, true);