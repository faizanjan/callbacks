const {getContent, 
    contentToUpperCase, 
    contentToLowerCase, 
    splitToSentences, 
    sortContent, 
    deleteAll} = require('../problem2');
let path = './lipsum.txt';

getContent(path,(data)=>{
    console.log("lipsum read")
    contentToUpperCase(data, (path)=>{
        console.log("converted to uppercase")
        getContent(path,(data)=>{
            console.log("Uppercase data fetched")
            contentToLowerCase(data, (lowerCaseData)=>{
                console.log("converted to lowercase")
                splitToSentences(lowerCaseData, (path)=>{
                    console.log("Split into sentences")
                    getContent(path,(data)=>{
                        console.log("Lowercase sentences data fetched")
                        sortContent(data, (allFileNames)=>{
                            console.log("data sorted")
                            deleteAll(allFileNames);
                            console.log('Done')
                        })
                    })
                })
            })
        })
    })
})