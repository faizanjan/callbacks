const fs = require('fs')

function getContent(path, cb){
    if(!fs.existsSync(path)) throw new Error ("Path deosn't exist");
    fs.readFile(path, 'utf-8', (err,data)=>{
        if (err) throw err;
        const content = data;
        cb(content);
    });
}

function contentToUpperCase(data, cb){
    const upperCaseData = data.toUpperCase();
    const newPath = 'upperCaseFile.txt'
    fs.writeFile(newPath, upperCaseData, (error)=>{
        if(error) throw error;
        fs.appendFile('filenames.txt', newPath+'\n', (err)=>{if(err) throw err});
        cb(newPath);
    });
}

function contentToLowerCase(data, cb){
    const lowerCaseData = data.toLowerCase();
    const newPath = 'lowerCaseFile.txt'

    fs.writeFile(newPath, lowerCaseData, (error)=>{
        if(error) throw error;
        fs.appendFile('filenames.txt', newPath+'\n', (err)=>{if(err) throw err});
        cb(lowerCaseData);
    });
}

function splitToSentences (data, cb){
    const newContent = data.split(/[.!?]\s/g).join('\n');
    const newPath = 'sentences.txt'
    fs.writeFile(newPath, newContent,(err)=>{
        if(err) throw err;
        fs.appendFile('filenames.txt', newPath+'\n', (err)=>{if(err) throw err});
        cb(newPath);
    })
}

function sortContent(data, cb){
    const sortedContent = data.split('\n').sort().join('\n');
    const newPath = 'sortedFile.txt'
    fs.writeFile(newPath, sortedContent,(err)=>{
        if(err) throw err;
        fs.appendFile('filenames.txt', newPath+'\n', (err)=>{
            if(err) throw err;
            cb('filenames.txt');
        });
    })
}

function deleteAll(namesFile){
    fs.readFile(namesFile, 'utf-8', (err, data)=>{
        if(err) throw err;
        else {
            const fileNames = data.split('\n');
            fileNames.forEach(file => {
                fs.unlink(file, ()=>{console.log(`${file} deleted`)})
            });
        }
    })
}

module.exports = {getContent, contentToUpperCase, contentToLowerCase, splitToSentences, sortContent, deleteAll}